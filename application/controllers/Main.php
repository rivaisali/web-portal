<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Main_model');
    }

    public function index()
    {

        $this->load->view('layout');
    }
    public function view()
    {
        $data['visimisi'] = $this->Main_model->get_post('visi-dan-misi');
        $data['progkebijakan'] = $this->Main_model->get_post('program-strategi-kebijakan');
        $data['sejarah'] = $this->Main_model->get_post('sejarah-gorontalo');
        $data['profil'] = $this->Main_model->get_post('profil-kota-gorontalo');
        $data['tentangkota'] = $this->Main_model->get_post('tentang-kota-gorontalo');
        $data['artilambang'] = $this->Main_model->get_post('arti-lambang');
        $data['opd'] = $this->Main_model->get_post('link-opd-3');
        $data['ringkasanlppd'] = $this->Main_model->get_post('ringkasan-lppd');
        $data['pengelolaankeu'] = $this->Main_model->get_post('pengelolaan-anggaran-daerah');
        $data['layananpublik'] = $this->Main_model->get_post('layanan-publik');

        $data['news'] = $this->Main_model->get_news();

        $this->load->view('main', $data);
    }
}
