<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="PORTAL RESMI PEMERINTAH KOTA GORONTALO">
	<meta name="author" content="DINAS KOMUNIKASI, INFORMATIKA DAN PERSANDIAN PEMERINTAH KOTA GORONTALO">
	<meta property="og:image" content="img/logo.png" />
	<meta property="og:description"
		content="Kota Gorontalo (Bahasa Gorontalo: Hulontalo, transliterasi: Kota Hulontalo) merupakan Ibu kota Provinsi Gorontalo, Indonesia. Kota Gorontalo merupakan kota terbesar dan terpadat penduduknya di wilayah Teluk Tomini (Teluk Gorontalo), sehingga menjadikan Kota Gorontalo sebagai pusat ekonomi, jasa dan perdagangan, pendidikan, hingga pusat penyebaran agama Islam di Kawasan Indonesia Timur." />
	<meta property="og:url" content="https://gorontalokota.go.id/" />
	<meta property="og:title" content="Kota Gorontalo Smart City" />
	<meta property="og:site_name" content="Kota Gorontalo Smart City" />
	<meta property="og:admins" content="DINAS KOMUNIKASI, INFORMATIKA DAN PERSANDIAN PEMERINTAH KOTA GORONTALO" />

	<link rel="icon" href="<?php base_url()?>assets/img/logo.png" type="image/x-icon">
	<title>Portal - Website Portal Resmi Pemerintah Kota Gorontalo</title>
	<!-- Bootstrap core CSS -->
	<link href="<?php base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
	<!-- Custom styles for this template -->
	<link href="<?php base_url()?>assets/css/style_pemkot.css" rel="stylesheet">
	<link href="<?php base_url()?>assets/css/hover.css" rel="stylesheet">
	<link href="<?php base_url()?>assets/css/breaking-news-ticker.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
</head>

<body>

	<header id="header">
		<img class="logo" src="<?php base_url()?>assets/img/kotalogo.png">
	</header>
	<!-- /header -->

	<!-- Begin page content -->
	<main>
		<div class="bg">
			<div class="dots_1"><img src="<?php base_url()?>assets/img/dots.png"></div>
			<div class="dots_2"><img src="<?php base_url()?>assets/img/dots.png"></div>
		</div>

		<div role="main" class="container" id="container">
			<div class="bn-breaking-news" id="newsTicker1">
				<div class="bn-label" style="background-color: #f8b33c;border-color:black;">BERITA TERBARU</div>
				<div class="bn-news">
					<ul>
					<?php foreach ($news as $n): ?>
						<li><a href="https://berita.gorontalokota.go.id/post/<?=$n['post_slug'];?>"><img src="<?=$n['post_thumb'];?>" height="40px" width="65px"><?=$n['post_title'];?></a></li>
					<?php endforeach;?>
					</ul>
				</div>
				<div class="bn-controls">
					<button><span class="bn-arrow bn-prev"></span></button>
					<button><span class="bn-action"></span></button>
					<button><span class="bn-arrow bn-next"></span></button>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-2 col-xs-6 hvr-bounce-in">
					<a href="https://berita.gorontalokota.go.id" target="_blank">
						<div class="box-menu">
							<img src="<?php base_url()?>assets/img/berita.png">
							<span>Berita</span>
						</div>
					</a>
				</div>
				<div class="col-md-2 col-xs-6 hvr-bounce-in">
					<div class="box-menu" data-toggle="modal" data-target="#modalMenuProfil">
						<img src="<?php base_url()?>assets/img/profile.png">
						<span>Profil Kota Gorontalo</span>
					</div>
				</div>
				<div class="col-md-2 col-xs-6 hvr-bounce-in">
					<a href="#">
						<div class="box-menu" data-toggle="modal" data-target="#modalMenuPemerintahan">
							<img src="<?php base_url()?>assets/img/egov.png">
							<span>Pemerintahan</span>
						</div>
					</a>
				</div>

				<div class="col-md-2 col-xs-6 hvr-bounce-in">
					<div class="box-menu">
						<img src="<?php base_url()?>assets/img/sarpras.png">
						<span>Sarana dan Prasarana</span>
					</div>
				</div>

				<div class="col-md-2 col-xs-6 hvr-bounce-in">
					<div class="box-menu" data-toggle="modal" data-target="#modalMenuLayananPublik">
						<img src="<?php base_url()?>assets/img/services.png">
						<span>Layanan Publik</span>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2 col-xs-6 hvr-bounce-in">
					<div class="box-menu">
						<img src="<?php base_url()?>assets/img/layanankeckel.png">
						<span>Layanan Kecamatan dan Kelurahan</span>
					</div>
				</div>
				<div class="col-md-2 col-xs-6 hvr-bounce-in">
					<div class="box-menu" data-toggle="modal" data-target="#modalMenuTransparansi">
						<img src="<?php base_url()?>assets/img/keuangan.png">
						<span>Transparansi</span>
					</div>
				</div>
				<div class="col-md-2 col-xs-6 hvr-bounce-in">
					<a href="https://lpse.gorontalokota.go.id/eproc4" target="_blank">
						<div class="box-menu">
							<img src="<?php base_url()?>assets/img/checklist.png">
							<span>Rencana Umum Pengadaan</span>
						</div>
					</a>
				</div>
				<div class="col-md-2 col-xs-6 hvr-bounce-in">
					<div class="box-menu" data-toggle="modal" data-target="#modalMenuOpd">
						<img src="<?php base_url()?>assets/img/opd.png">
						<span>Perangkat Daerah</span>
					</div>
				</div>
				<div class="col-md-2 col-xs-6 hvr-bounce-in">
					<a href="http://edata.gorontalokota.go.id/" target="_blank">
						<div class="box-menu">
							<img src="<?php base_url()?>assets/img/statistik.png">
							<span>Data dan Statistik</span>
						</div>
					</a>
				</div>


			</div>
		</div>
		<div class="control">
			<button class="btn btn-primary" id="slideLeft"><i class="fa fa-chevron-left"></i></button>&nbsp;
			&nbsp;<button class="btn btn-primary" id="slideRight"><i class="fa fa-chevron-right"></i></button>
		</div>
	</main>

	<footer class="footer"></footer>

	<!-- Modal Pemerintahan -->
	<div class="modal fade" id="modalMenuPemerintahan" tabindex="-1" role="dialog" aria-labelledby="modalMenuLabel"
		aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modalMenuLabel">PEMERINTAHAN</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="modal-body" id="body">


						<div class="bd-example" role="tabpanel">
							<div class="row">
								<div class="col-sm-3">
									<div class="list-group" id="list-tab" role="tablist">
										<a class="list-group-item list-group-item-action active show"
											id="list-settings-list" data-toggle="tab" href="#" role="tab"
											aria-controls="list-visi-dan-misi" aria-selected="true">Pemerintahan</a>
										<a class="list-group-item list-group-item-action" id="list-visi-dan-misi-list"
											data-toggle="tab" href="#list-visi-dan-misi" role="tab"
											aria-controls="list-home" aria-selected="false">Visi & Misi</a>
										<a class="list-group-item list-group-item-action"
											id="list-program-strategi-kebijakan-list" data-toggle="tab"
											href="#list-program-strategi-kebijakan" role="tab"
											aria-controls="list-program-strategi-kebijakan"
											aria-selected="false">Program Strategi Kebijakan</a>

									</div>
								</div>
								<div class="col-sm-8">
									<div class="tab-content" id="nav-tabContent">

										<div class="tab-pane fade active" id="list-visi-dan-misi" role="tabpanel"
											aria-labelledby="list-visi-dan-misi-list">
											<?=$visimisi['post_content'];?>
										</div>
										<div class="tab-pane fade" id="list-program-strategi-kebijakan" role="tabpanel"
											aria-labelledby="list-program-strategi-kebijakan-list">
											<?=$progkebijakan['post_content'];?>>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>

					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- Modal Profil -->
	<div class="modal fade" id="modalMenuProfil" tabindex="-1" role="dialog" aria-labelledby="modalMenuLabel"
		aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modalMenuLabel">PROFIL</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="modal-body" id="body">

						<div class="bd-example" role="tabpanel">
							<div class="row">
								<div class="col-sm-3">
									<div class="list-group" id="list-tab" role="tablist">
										<a class="list-group-item list-group-item-action active show"
											id="list-settings-list" data-toggle="tab" href="#" role="tab"
											aria-controls="list-sejarah" aria-selected="true">Profil</a>
										<a class="list-group-item list-group-item-action" id="list-sejarah-list"
											data-toggle="tab" href="#list-sejarah" role="tab"
											aria-controls="list-sejarah" aria-selected="false">Sejarah</a>
										<a class="list-group-item list-group-item-action" id="list-profil-list"
											data-toggle="tab" href="#list-profil" role="tab" aria-controls="list-profil"
											aria-selected="false">Profil Kota Gorontalo</a>
										<a class="list-group-item list-group-item-action" id="list-tentang-list"
											data-toggle="tab" href="#list-tentang" role="tab"
											aria-controls="list-tentang" aria-selected="false">Tentang Kota
											Gorontalo</a>
										<a class="list-group-item list-group-item-action" id="list-lambang-list"
											data-toggle="tab" href="#list-lambang" role="tab"
											aria-controls="list-lambang" aria-selected="false">Arti Lambang</a>
									</div>
								</div>
								<div class="col-sm-8">
									<div class="tab-content" id="nav-tabContent">

										<div class="tab-pane fade active" id="list-sejarah" role="tabpanel"
											aria-labelledby="list-sejarah-list">
											<?=$sejarah['post_content'];?>
										</div>
										<div class="tab-pane fade" id="list-profil" role="tabpanel"
											aria-labelledby="list-profil-list">
											<?=$profil['post_content'];?>
										</div>
										<div class="tab-pane fade" id="list-tentang" role="tabpanel"
											aria-labelledby="list-tentang-list">
											<?=$tentangkota['post_content'];?>
										</div>

										<div class="tab-pane fade" id="list-lambang" role="tabpanel"
											aria-labelledby="list-lambang-list">
											<?=$artilambang['post_content'];?>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal Transparansi -->
	<div class="modal fade" id="modalMenuTransparansi" tabindex="-1" role="dialog" aria-labelledby="modalMenuLabel"
		aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modalMenuLabel">Transparansi</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="modal-body" id="body">

						<div class="bd-example" role="tabpanel">
							<div class="row">
								<div class="col-sm-3">
									<div class="list-group" id="list-tab" role="tablist">
										<a class="list-group-item list-group-item-action active show"
											id="list-settings-list" data-toggle="tab" href="#" role="tab"
											aria-controls="list-sejarah" aria-selected="true">Transparansi</a>
										<a class="list-group-item list-group-item-action" id="list-pengelolaankeu-list"
											data-toggle="tab" href="#list-pengelolaankeu" role="tab"
											aria-controls="list-lppd" aria-selected="false">Pengelolaan Anggaran
											Daerah</a>
										<a class="list-group-item list-group-item-action" id="list-lppd-list"
											data-toggle="tab" href="#list-lppd" role="tab" aria-controls="list-lppd"
											aria-selected="false">Ringkasan LPPD</a>
									</div>
								</div>
								<div class="col-sm-8">
									<div class="tab-content" id="nav-tabContent">

										<div class="tab-pane fade active" id="list-pengelolaankeu" role="tabpanel"
											aria-labelledby="list-pengelolaankeu-list">
											<?=$pengelolaankeu['post_content'];?>
										</div>
										<div class="tab-pane fade" id="list-lppd" role="tabpanel"
											aria-labelledby="list-lppd-list">
											<?=$ringkasanlppd['post_content'];?>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal OPD -->
	<div class="modal fade" id="modalMenuOpd" tabindex="-1" role="dialog" aria-labelledby="modalMenuLabel"
		aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modalMenuLabel">Organisasi Perangkat Daerah</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="modal-body" id="body">

						<div class="bd-example" role="tabpanel">
							<div class="row">

								<div class="col-sm-12">
									<div class="tab-content" id="nav-tabContent">
										<?=$opd['post_content'];?>


									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal Profil -->
	<div class="modal fade" id="modalMenuLayananPublik" tabindex="-1" role="dialog" aria-labelledby="modalMenuLabel"
		aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modalMenuLabel">Layanan Publik</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="modal-body" id="body">

						<div class="bd-example" role="tabpanel">
							<div class="row">
								<div class="col-sm-12">
									<div class="tab-content" id="nav-tabContent">
										<?=$layananpublik['post_content'];?>

									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="<?=base_url()?>assets/js/breaking-news-ticker.min.js"></script>
	<script src="<?=base_url()?>assets/js/app.js"></script>
</body>

</html>
