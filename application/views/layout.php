<html lang="en" class="no-js">
   <head>
      <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="PORTAL RESMI PEMERINTAH KOTA GORONTALO">
	<meta name="author" content="DINAS KOMUNIKASI, INFORMATIKA DAN PERSANDIAN PEMERINTAH KOTA GORONTALO">
	<meta property="og:image" content="img/logo.png" />
	<meta property="og:description"
		content="Kota Gorontalo (Bahasa Gorontalo: Hulontalo, transliterasi: Kota Hulontalo) merupakan Ibu kota Provinsi Gorontalo, Indonesia. Kota Gorontalo merupakan kota terbesar dan terpadat penduduknya di wilayah Teluk Tomini (Teluk Gorontalo), sehingga menjadikan Kota Gorontalo sebagai pusat ekonomi, jasa dan perdagangan, pendidikan, hingga pusat penyebaran agama Islam di Kawasan Indonesia Timur." />
	<meta property="og:url" content="https://gorontalokota.go.id/" />
	<meta property="og:title" content="Kota Gorontalo Smart City" />
	<meta property="og:site_name" content="Kota Gorontalo Smart City" />
	<meta property="og:admins" content="DINAS KOMUNIKASI, INFORMATIKA DAN PERSANDIAN PEMERINTAH KOTA GORONTALO" />

	<link rel="icon" href="<?php base_url()?>assets/img/logo.png" type="image/x-icon">
	<title>Portal - Website Portal Resmi Pemerintah Kota Gorontalo</title>
      <style>
      html body {width: 100%;height: 100%;padding: 0px;margin: 0px;overflow: hidden;font-family: arial;font-size: 10px;color: #6e6e6e;background-color: #000;} #preview-frame {width: 100%;background-color: #fff;}</style>
      <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
      <script>
         //function to fix height of iframe!
         var calcHeight = function() {
           //var headerDimensions = 0; //$('#header-bar').height();
           $('#preview-frame').height($(window).height());
         }

         $(document).ready(function() {
           calcHeight();
         });

         $(window).resize(function() {
           calcHeight();
         }).load(function() {
           calcHeight();
         });
      </script>
   </head>
   <body oncontextmenu="return false" onkeydown="return false;" onmousedown="return false;">
      <iframe id="preview-frame" src="<?=base_url()?>main" name="preview-frame" frameborder="0" noresize="noresize" style="height: 902px;">
      </iframe>
   </body>
</html>
